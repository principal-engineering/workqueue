package workqueue

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx/v5/pgxpool"
)

func (p *FactorialCalculationEventProducer) Produce(ctx context.Context, event FactorialCalculationEvent) error {
	b, err := json.Marshal(event)
	if err != nil {
		return err
	}

	return p.produce(ctx, FactorialCalculationEventTopic, b)
}

func NewFactorialCalculationEventProducer(dbpool *pgxpool.Pool) *FactorialCalculationEventProducer {
	return &FactorialCalculationEventProducer{
		producer{
			dbpool: dbpool,
		},
	}
}

type FactorialCalculationEventProducer struct {
	producer
}
