package workqueue

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

func (c *FibonacciCalculationEventConsumer) Consume(ctx context.Context, f func(ctx context.Context, event FibonacciCalculationEvent) error) error {
ctxdone:
	for {
		select {
		case <-ctx.Done():
			break ctxdone
		case <-time.After(1 * time.Second): // fixme possible memory leak
			id, payload, err := c.consumer.peek(ctx, c.worker)
			switch {
			case errors.Is(err, pgx.ErrNoRows):
				fmt.Fprintf(os.Stderr, "topic emtpy: %s\n", c.consumer.topic)
				continue
			case err != nil:
				return err
			}

			var event FibonacciCalculationEvent
			if err := json.Unmarshal(payload, &event); err != nil {
				return err
			}

			if err := f(ctx, event); err != nil {
				return err
			}

			if err := c.dequeue(ctx, id); err != nil {
				return err
			}
		}
	}

	return nil
}

func NewFibonacciCalculationEventConsumer(dbpool *pgxpool.Pool, worker string) *FibonacciCalculationEventConsumer {
	return &FibonacciCalculationEventConsumer{
		consumer: consumer{
			dbpool: dbpool,
			topic:  FibonacciCalculationEventTopic,
		},
		worker: worker,
	}
}

type FibonacciCalculationEventConsumer struct {
	consumer

	worker string
}
