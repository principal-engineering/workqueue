package workqueue

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

func (c *consumer) peek(ctx context.Context, worker string) (id int, payload []byte, err error) {
	sql := `with task as (select id
              from workqueue.workqueue
              where locked_by is null
                    -- or locked_at < now() - :retry_timeout
                    and topic = $1
              order by id for update skip locked
              limit 1)
update workqueue.workqueue q
set locked_by = $2,
    locked_at = now()
from task
where task.id = q.id
returning q.id, q.payload;
`

	err = c.dbpool.QueryRow(ctx, sql, c.topic, worker).Scan(&id, &payload)

	return id, payload, err
}

func (c *consumer) dequeue(ctx context.Context, id int) error {
	sqlDeque := `with dequeued as (
  delete from workqueue.workqueue
    where id = $1
    returning id, enqueued_at, topic, payload, locked_by)
insert
into workqueue.workqueue_history (id, enqueued_at, topic, payload, processed_by)
select dequeued.id, dequeued.enqueued_at, dequeued.topic, dequeued.payload, dequeued.locked_by
from dequeued;
`

	if _, err := c.dbpool.Exec(ctx, sqlDeque, id); err != nil {
		return err
	}

	return nil
}

type consumer struct {
	dbpool *pgxpool.Pool
	topic  string
}
