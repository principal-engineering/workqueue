package workqueue

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx/v5/pgxpool"
)

func (p *FibonacciCalculationEventProducer) Produce(ctx context.Context, event FibonacciCalculationEvent) error {
	b, err := json.Marshal(event)
	if err != nil {
		return err
	}

	return p.produce(ctx, FibonacciCalculationEventTopic, b)
}

func NewFibonacciCalculationEventProducer(dbpool *pgxpool.Pool) *FibonacciCalculationEventProducer {
	return &FibonacciCalculationEventProducer{
		producer{
			dbpool: dbpool,
		},
	}
}

type FibonacciCalculationEventProducer struct {
	producer
}
