package workqueue

const FactorialCalculationEventTopic = `factorial_calculation_event_topic`

type FactorialCalculationEvent struct {
	Num int `json:"num"`
}
