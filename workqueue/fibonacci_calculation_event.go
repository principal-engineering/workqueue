package workqueue

const FibonacciCalculationEventTopic = `fibonacci_calculation_event_topic`

type FibonacciCalculationEvent struct {
	Num int `json:"num"`
}
