package workqueue

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

func (p *producer) produce(ctx context.Context, topic string, payload []byte) error {
	sql := `insert into workqueue.workqueue (topic, payload) values ($1, $2)`

	if _, err := p.dbpool.Exec(ctx, sql, topic, payload); err != nil {
		return err
	}

	return nil
}

type producer struct {
	dbpool *pgxpool.Pool
}
