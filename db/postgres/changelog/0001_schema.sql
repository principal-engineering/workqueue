create schema workqueue;

create table workqueue.workqueue
(
  id          bigserial primary key,
  enqueued_at timestamptz not null default now(),
  topic       text        not null,
  payload     jsonb       not null,
  locked_at   timestamptz,
  locked_by   text
);

create table workqueue.workqueue_history
(
  id           bigint      not null,
  enqueued_at  timestamptz not null,
  topic        text        not null,
  payload      jsonb       not null,
  processed_by text        not null,
  processed_at timestamptz not null default now()
);
