insert into workqueue.workqueue (topic, payload)
values ('foo-bar', jsonb_build_object('value', 'foo')),
       ('foo-bar', jsonb_build_object('value', 'bar')),
       ('foo-bar', jsonb_build_object('value', 'baz'))
;
