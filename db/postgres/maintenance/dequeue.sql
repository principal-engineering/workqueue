with dequeued as (
  delete from workqueue.workqueue
    where id = :id
    returning id, enqueued_at, topic, payload, locked_by)
insert
into workqueue.workqueue_history (id, enqueued_at, topic, payload, processed_by)
select dequeued.id, dequeued.enqueued_at, dequeued.topic, dequeued.payload, dequeued.locked_by
from dequeued;
