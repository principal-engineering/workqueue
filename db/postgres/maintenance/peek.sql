with task as (select id
              from workqueue.workqueue
              where locked_by is null
                    -- or locked_at < now() - :retry_timeout
              order by id for update skip locked
              limit 1)
update workqueue.workqueue q
set locked_by = :worker_name,
    locked_at = now()
from task
where task.id = q.id
returning q.id, q.payload;
