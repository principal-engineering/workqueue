package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"workqueue/workqueue"

	"github.com/jackc/pgx/v5/pgxpool"
)

func main() {
	ctx := context.Background()

	dbpool, err := pgxpool.New(ctx, "postgres://app:secret@localhost/app?sslmode=disable")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create connection pool: %v\n", err)
		os.Exit(1)
	}
	defer dbpool.Close()

	factProducer := workqueue.NewFactorialCalculationEventProducer(dbpool)

	_ = factProducer.Produce(ctx, workqueue.FactorialCalculationEvent{Num: 1})
	_ = factProducer.Produce(ctx, workqueue.FactorialCalculationEvent{Num: 5})
	_ = factProducer.Produce(ctx, workqueue.FactorialCalculationEvent{Num: 10})

	fibProducer := workqueue.NewFibonacciCalculationEventProducer(dbpool)

	_ = fibProducer.Produce(ctx, workqueue.FibonacciCalculationEvent{Num: 5})
	_ = fibProducer.Produce(ctx, workqueue.FibonacciCalculationEvent{Num: 10})
	_ = fibProducer.Produce(ctx, workqueue.FibonacciCalculationEvent{Num: 25})

	factConsumer := workqueue.NewFactorialCalculationEventConsumer(dbpool, "worker-fact") // todo os.Hostname()

	go func() {
		err = factConsumer.Consume(ctx, func(ctx context.Context, event workqueue.FactorialCalculationEvent) error {
			fmt.Printf("fact: %#v\n", event)
			return nil
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Consume error: %v\n", err)
		}
	}()

	go func() {
		fibConsumer := workqueue.NewFibonacciCalculationEventConsumer(dbpool, "worker-fib")

		err = fibConsumer.Consume(ctx, func(ctx context.Context, event workqueue.FibonacciCalculationEvent) error {
			fmt.Printf("fib: %#v\n", event)
			return nil
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Consume error: %v\n", err)
		}
	}()

	// todo block
	time.Sleep(10 * time.Second)
}
